package ch.hslu.ai.connect4.common;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MiniMaxTest {


    private Board board;

    private MiniMax miniMax;

    @Mock
    private Heuristic heuristic;

    @Before
    public void setUp() {
        this.board = new Board(7, 6, 's', '9', true);
        miniMax = new MiniMax();

        miniMax.setHeuristic(heuristic);

    }

    @Test
    public void testMiniMaxZero() throws Exception {

        when(heuristic.rateBoard(board)).thenReturn(-100);

        assertEquals(-100, miniMax.miniMax(0, board));
    }

    @Test
    public void testMiniMaxSimple() throws Exception {



    }


}