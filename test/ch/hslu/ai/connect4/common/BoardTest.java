package ch.hslu.ai.connect4.common;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BoardTest {

    private Board board;

    @Before
    public void setUp() throws Exception {
        board = new Board(7, 6, 's', '9', true);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testColumnNumberTooHigh() {

        this.board.makeMove(7);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testColumnNumberTooLow() {

        this.board.makeMove(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testColumnIsFull() {
        for (int i = 0; i < 7; i++) {
            board.makeMove(4);
        }
    }


    @Test
    public void testInsertFirstRow() {

        board.makeMove(2);

        assertEquals('s', board.getFields()[2][0]);

    }

    @Test
    public void testInsertMiddleRow() {

        board.makeMove(4);
        board.makeMove(4);
        board.makeMove(4);
        board.makeMove(4);

        char[][] fields = board.getFields();

        assertEquals('s', fields[4][0]);
        assertEquals('9', fields[4][1]);
        assertEquals('s', fields[4][2]);
        assertEquals('9', fields[4][3]);
    }

    @Test
    public void testInsertTopRow() {

        for (int i = 0; i < 6; i++) {
            board.makeMove(4);
        }

        char[][] fields = board.getFields();

        assertEquals('9', fields[4][5]);
    }



    @Test
    public void testPrintBoardFilled() {

        for (int i = 0; i < 5; i++) {
            board.makeMove(3);
        }


        String expected =
                "#########\n" +
                        "#-------#\n" +
                        "#---s---#\n" +
                        "#---9---#\n" +
                        "#---s---#\n" +
                        "#---9---#\n" +
                        "#---s---#\n" +
                        "#########\n\n";

        assertEquals(expected, this.board.toString());
    }

    @Test
    public void testPrintBoardEmpty() {


        String expected =
                "#########\n" +
                        "#-------#\n" +
                        "#-------#\n" +
                        "#-------#\n" +
                        "#-------#\n" +
                        "#-------#\n" +
                        "#-------#\n" +
                        "#########\n\n";

        assertEquals(expected, this.board.toString());
    }

    @Test
    public void testEmptyBoardGeneration() {

        char[][] fields = board.getFields();


        for (int i = 0; i < 7; i++) {
            for (int k = 0; k < 6; k++) {
                assertEquals('-', fields[i][k]);
            }
        }
    }

    @Test
    public void testEqualsWithInsertedRow() {
        board.makeMove(2);

        Board emptyBoard = new Board(7, 6, 's', '9', true);

        assertFalse(board.equals(emptyBoard));
    }

    @Test
    public void testEqualsEmpty() {

        Board emptyBoard = new Board(7, 6, 's', '9', true);

        assertTrue(board.equals(emptyBoard));
    }


    @Test
    public void testEqualsBothInserted() {

        Board otherBoard = new Board(7, 6, 's', '9', true);
        board.makeMove(4);
        otherBoard.makeMove(4);

        assertTrue(board.equals(otherBoard));
    }

    @Test
    public void testRevertMove() {
        Board emptyBoard = new Board(7, 6, 's', '9', true);

        board.makeMove(4);
        board.revertMove(4);


        assertEquals(emptyBoard, board);

    }

    @Test
    public void testRevertMoveManyMoves() {
        board.makeMove(4);
        board.makeMove(4);
        board.makeMove(4);
        board.makeMove(5);
        board.makeMove(3);
        board.makeMove(2);

        String previous = board.toString();

        board.makeMove(5);

        assertNotSame(previous, board.toString());
        board.revertMove(5);

        assertEquals(previous, board.toString());


    }

    @Test(expected = IllegalArgumentException.class)
    public void testRevertMoveEmpty() {
        Board emptyBoard = new Board(7, 6, 's', '9', true);

        board.revertMove(3);

        assertEquals(emptyBoard, board);

    }

    @Test
    public void testRevertMoveUnequals() {
        Board otherBoard = new Board(7, 6, 's', '9', true);

        board.makeMove(4);
        otherBoard.makeMove(5);

        board.revertMove(4);

        assertNotSame(otherBoard, board);

    }


    @Test
    public void outcometestEnemy() {
        board.makeMove(4); // me
        board.makeMove(0); // enemy
        board.makeMove(5);
        board.makeMove(0);
        board.makeMove(4);
        board.makeMove(0);
        board.makeMove(5);
        board.makeMove(0);


        Outcome outcome = board.getOutcome();

        assertEquals(Outcome.ENEMY_WINS, outcome);
    }


    @Test
    public void outcometestIWin() {
        board.makeMove(2); // me
        board.makeMove(1); // enemy
        board.makeMove(2);
        board.makeMove(0);
        board.makeMove(2);
        board.makeMove(0);
        board.makeMove(2);
        board.makeMove(0);


        Outcome outcome = board.getOutcome();

        assertEquals(Outcome.I_WIN, outcome);
    }

    @Test
    public void testPossibleMovesEmpty() {
        List<Integer> possibleMoves = board.getListOfNextMoves();

        assertEquals(7, possibleMoves.size());
    }

    @Test
    public void testPossibleMovesOneColumnFull() {

        for (int i = 0; i < 6; i++){
            board.makeMove(0);
        }
        List<Integer> possibleMoves = board.getListOfNextMoves();

        assertEquals(6, possibleMoves.size());
    }
    @Test
    public void testPossibleMovesOneRowFull() {

        for (int i = 0; i < 6; i++){
            board.makeMove(i);
        }
        for (int i = 0; i < 6; i++){
            board.makeMove(i);
        }
        List<Integer> possibleMoves = board.getListOfNextMoves();

        assertEquals(7, possibleMoves.size());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRevertMoveInvalid() {
        Board otherBoard = new Board(7, 6, 's', '9', true);

        board.makeMove(4);
        board.revertMove(5);

        assertNotSame(otherBoard, board);

    }


}