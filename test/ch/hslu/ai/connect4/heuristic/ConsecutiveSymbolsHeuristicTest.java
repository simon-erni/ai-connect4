package ch.hslu.ai.connect4.heuristic;

import ch.hslu.ai.connect4.common.Board;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ConsecutiveSymbolsHeuristicTest {

    private Board board;

    private ConsecutiveSymbolsHeuristic symbolsHeuristic;

    @Before
    public void setUp() {
        this.board = new Board(7, 6, 'm', 'o', true);
        this.symbolsHeuristic = new ConsecutiveSymbolsHeuristic();
    }

    @Test
    public void testRateBoardSimpleHorizontal() throws Exception {
        board.makeMove(0);
        board.makeMove(0);

        board.makeMove(1);
        board.makeMove(1);

        board.makeMove(2);
        board.makeMove(2);

        /*
         -------
         -------
         -------
         -------
         ooo----
         mmm---- 
         */
        assertEquals(1030, symbolsHeuristic.rateBoard(board));
    }

    @Test
    public void testRateBordSimpleVertical() throws Exception {
        board.makeMove(0);
        board.makeMove(1);

        board.makeMove(0);
        board.makeMove(1);

        board.makeMove(0);
        board.makeMove(1);

        /*
         -------
         -------
         -------
         mo-----
         mo-----
         mo-----
         */
        assertEquals(1030, symbolsHeuristic.rateBoard(board));

    }

    @Test
    public void testRateBordSimpleDiagonal1() throws Exception {
        board.makeMove(0);
        board.makeMove(2);

        board.makeMove(0);
        board.makeMove(0);

        board.makeMove(1);
        board.makeMove(1);

        /*
         -------
         -------
         -------
         o------
         mo-----
         mmo----
         */
        assertEquals(-410, symbolsHeuristic.rateBoard(board));
    }

    @Test
    public void testRateBordSimpleDiagonal2() throws Exception {
        board.makeMove(6);
        board.makeMove(4);

        board.makeMove(5);
        board.makeMove(5);

        board.makeMove(6);
        board.makeMove(6);

        /*
         -------
         -------
         -------
         ------o
         -----om
         ----omm
         */
        symbolsHeuristic.rateBoard(board);
        assertEquals(-410, symbolsHeuristic.rateBoard(board));
    }

    @Test
    public void testRateBordHorizontalWithSpace() throws Exception {
        board.makeMove(0);
        board.makeMove(0);

        board.makeMove(1);
        board.makeMove(1);

        board.makeMove(3);
        board.makeMove(3);

        /*
         -------
         -------
         -------
         -------
         oo-o---
         mm-m---
         */
        assertEquals(1930, symbolsHeuristic.rateBoard(board));
    }
}
