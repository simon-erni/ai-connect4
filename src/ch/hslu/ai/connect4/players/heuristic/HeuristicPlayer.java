package ch.hslu.ai.connect4.players.heuristic;

import ch.hslu.ai.connect4.Player;
import ch.hslu.ai.connect4.common.Board;
import ch.hslu.ai.connect4.common.Heuristic;
import ch.hslu.ai.connect4.common.MiniMax;

public class HeuristicPlayer extends Player {
    private final Heuristic heuristic;

    /**
     * Constructor:
     *
     * @param name The name of this player.
     */
    public HeuristicPlayer(String name, Heuristic heuristic) {
        super(name);
        this.heuristic = heuristic;
    }

    @Override
    public int play(char[][] fields) {
        Board board = new Board(fields, getSymbol(), true);


        MiniMax miniMax = new MiniMax();

        return miniMax.getMove(heuristic, board, 4);
    }
}
