package ch.hslu.ai.connect4.players.simple;

import ch.hslu.ai.connect4.Player;
import ch.hslu.ai.connect4.common.Board;

public class SingleColumnPlayer extends Player {
    /**
     * Constructor:
     *
     * @param name The name of this player.
     */
    public SingleColumnPlayer(String name) {
        super(name);
    }

    @Override
    public int play(char[][] fields) {


        int width = fields.length;

        for (int i = 0; i < width; i++) {
            if (fields[i][0] == '-') {
                return i;
            }
        }

        Board board = new Board(fields, getSymbol(), true);
        System.out.println(board);

        return 0;

    }
}
