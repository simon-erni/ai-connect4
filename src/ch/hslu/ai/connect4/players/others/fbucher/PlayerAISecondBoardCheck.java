/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Florian Bucher
 */
public class PlayerAISecondBoardCheck extends PlayerAIBase {

    private final Heuristic heuristic = new PatternHeuristic();

    public PlayerAISecondBoardCheck(String name, char symbol) {
        super(name, symbol);
    }

    @Override
    protected Heuristic initHeuristic() {
        return heuristic;
    }

    @Override
    protected int getMinMaxDepth() {
        return 5;
    }

    public class PatternHeuristic implements Heuristic {

        @Override
        public int getHeuristic(Board board) {
            int hScore = 0;
            char gameWinner = board.gameSetteledBy();

            if (gameWinner == board.oppSymbol) {
                // worst possible move (must be better than MIN_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MIN_VALUE + 1;
            } else if (gameWinner == board.ownSymbol) {
                // best possible move (must be worse than MAX_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MAX_VALUE - 1;
            } else {
                hScore = calcBoard(board);
            }

            return hScore;
        }

        private int calcBoard(Board board) {
            // prepare list for checks
            char[][] connectCellCheckList = {{}, {}, {}, {}};

            String line;
            Map<String, Integer> pattern = getPatternList(board.emptySymbol, board.ownSymbol, board.oppSymbol);

            int patternScore = 0;
            int score = 0;
            boolean diag5;
            boolean diag6;

            // check all fields and look for connect 4s
            for (int col = 0;
                 col < board.boardCols;
                 col++) {
                for (int row = board.boardRows - 1; row >= 0; row--) {
                    connectCellCheckList = new char[][]{{}, {}, {}, {}};

                    // check entire column
                    if (row > 4) {
                        connectCellCheckList[0] = new char[]{
                                board.get(col, row),
                                board.get(col, row - 1),
                                board.get(col, row - 2),
                                board.get(col, row - 3),
                                board.get(col, row - 4),
                                board.get(col, row - 5)
                        };
                    }
                    // check entire row
                    if (col < board.boardCols - 6) {
                        connectCellCheckList[1] = new char[]{
                                board.get(col, row),
                                board.get(col + 1, row),
                                board.get(col + 2, row),
                                board.get(col + 3, row),
                                board.get(col + 4, row),
                                board.get(col + 5, row),
                                board.get(col + 6, row)
                        };
                    }
                    // check diagnoal l-2-r
                    if ((col > 2 && row == board.boardRows - 1) || (col == board.boardCols - 1 && row > 2)) {
                        diag5 = (col > 3 && row == board.boardRows - 1) || (col == board.boardCols - 1 && row > 3);
                        diag6 = (col > 4 && row == board.boardRows - 1);
                        connectCellCheckList[2] = new char[]{
                                board.get(col, row),
                                board.get(col - 1, row - 1),
                                board.get(col - 2, row - 2),
                                board.get(col - 3, row - 3),
                                (diag5 ? board.get(col - 4, row - 4) : '\0'),
                                (diag6 ? board.get(col - 5, row - 5) : '\0')
                        };
                    }
                    // check diagnoal l-2-r
                    if ((col < board.boardCols - 3 && row == board.boardRows - 1) || (col == 0 && row > 2)) {
                        diag5 = (col < board.boardCols - 4 && row == board.boardRows - 1) || (col == 0 && row > 3);
                        diag6 = (col < board.boardCols - 5 && row == board.boardRows - 1);
                        connectCellCheckList[3] = new char[]{
                                board.get(col, row),
                                board.get(col + 1, row - 1),
                                board.get(col + 2, row - 2),
                                board.get(col + 3, row - 3),
                                (diag5 ? board.get(col + 4, row - 4) : '\0'),
                                (diag6 ? board.get(col + 5, row - 5) : '\0')
                        };
                    }

                    for (char[] cells : connectCellCheckList) {
                        if (cells.length >= 4) {
                            line = String.copyValueOf(cells);
                            for (String key : pattern.keySet()) {
                                if (line.contains(key)) {
                                    score += pattern.get(key);
                                    //System.out.println("Pattern [" + key + "," + pattern.get(key) + "]\t found in " + line);
                                }
                            }
                        }
                    }
                }
            }

            return score;
        }

        /**
         * Get a Map pattern and its winning values.
         *
         * @param eSymb  symbol of empty field
         * @param owSymb symbol of player
         * @param opSymb symbol of opponent
         * @return the list of patterns
         */
        private Map<String, Integer> getPatternList(char eSymb, char owSymb, char opSymb) {
            Map<String, Integer> pat = new HashMap<>();
            pat.put("" + eSymb + owSymb + owSymb + owSymb + eSymb, 50021);      // -xxx-
            pat.put("" + eSymb + opSymb + opSymb + opSymb + eSymb, -50021);    // -ooo-
            pat.put("" + eSymb + owSymb + owSymb + owSymb, 1009);               // -xxx
            pat.put("" + eSymb + opSymb + opSymb + opSymb, -1009);              // -ooo
            // x-xx
            // o-oo
            // xx-x
            // oo-o
            pat.put("" + owSymb + owSymb + owSymb + eSymb, 1009);               // xxx-
            pat.put("" + opSymb + opSymb + opSymb + eSymb, -1009);              // ooo-
            pat.put("" + owSymb + owSymb + eSymb + eSymb, 101);                 // xx--
            pat.put("" + opSymb + opSymb + eSymb + eSymb, -101);                // oo--
            pat.put("" + eSymb + owSymb + owSymb + eSymb, 211);                 // -xx-
            pat.put("" + eSymb + opSymb + opSymb + eSymb, -211);                // -oo-
            pat.put("" + eSymb + eSymb + owSymb + owSymb, 101);                 // --xx
            pat.put("" + eSymb + eSymb + opSymb + opSymb, -101);                // --oo
            // x--x
            // o--o
            // -x-x
            // -o-o
            // x-x-
            // o-o-
            pat.put("" + owSymb + eSymb + eSymb + eSymb, 1);                    // x---
            pat.put("" + opSymb + eSymb + eSymb + eSymb, -1);                   // o---
            pat.put("" + eSymb + owSymb + eSymb + eSymb, 2);                    // -x--
            pat.put("" + eSymb + opSymb + eSymb + eSymb, -2);                   // -o--
            pat.put("" + eSymb + eSymb + owSymb + eSymb, 2);                    // --x-
            pat.put("" + eSymb + eSymb + opSymb + eSymb, -2);                   // --o-
            pat.put("" + eSymb + eSymb + eSymb + owSymb, 1);                    // ---x
            pat.put("" + eSymb + eSymb + eSymb + opSymb, -1);                   // ---o
            return pat;
        }
    }

}
