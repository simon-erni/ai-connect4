/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

/**
 * @author Florian Bucher
 */
public class PlayerAISecondRandom extends PlayerAIBase {

    private final Heuristic heuristic = new RandomHeuristic();

    public PlayerAISecondRandom(String name, char symbol) {
        super(name, symbol);
    }

    @Override
    protected Heuristic initHeuristic() {
        return heuristic;
    }

    @Override
    protected int getMinMaxDepth() {
        return 7;
    }

    private class RandomHeuristic implements Heuristic {

        @Override
        public int getHeuristic(Board board) {
            return (int) (Math.random() * 100 - 50);
        }
    }

}
