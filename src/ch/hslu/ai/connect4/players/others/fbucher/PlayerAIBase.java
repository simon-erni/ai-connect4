/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

import ch.hslu.ai.connect4.Player;
import ch.hslu.ai.connect4.players.others.fbucher.MinMaxTree.Node;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Florian Bucher
 */
public abstract class PlayerAIBase extends Player {

    private ExecutorService pool = Executors.newFixedThreadPool(7);

    public PlayerAIBase(String name, char symbol) {
        super(name);
    }

    @Override
    public int play(char[][] newBoard) {
        // get board

        Board board = new Board(newBoard, getSymbol());
        MinMaxTree minMaxTree = new MinMaxTree(new PlayerMove(0, 0, '$'), false);

        // comits worker tasks
        Map<PlayerMove, Future<Integer>> minimaxResults = new HashMap<>();
        for (PlayerMove move : board.getListOfNextMoves(true)) {
            MiniMaxWorker worker = new MiniMaxWorker(
                    board.getClone(),
                    minMaxTree.getRoot(),
                    move,
                    getMinMaxDepth(),
                    false,
                    initHeuristic());
            minimaxResults.put(move, pool.submit(worker));
        }

        // executes
        int alpha = Integer.MIN_VALUE;
        Integer score = Integer.MIN_VALUE;
        PlayerMove bestMove = new PlayerMove(-1, -1, '-');
        try {
            for (Map.Entry<PlayerMove, Future<Integer>> minimaxResult : minimaxResults.entrySet()) {
                score = minimaxResult.getValue().get();
                if (score > alpha) { //maximizing
                    alpha = score;
                    bestMove = minimaxResult.getKey();
                }
            }
        } catch (InterruptedException iEx) {
            throw new RuntimeException("Worker has been interrupted:", iEx);
        } catch (ExecutionException eEx) {
            throw new RuntimeException("Worker has thrown an exception", eEx);
        }

        // print trace
        //minMaxTree.print();
        // before last move, cleare woorker pool
        return bestMove.col;
    }

    /**
     * Initializes the heuristic used
     *
     * @return the heuristic used for player.
     */
    protected abstract Heuristic initHeuristic();

    /**
     * get the value for the depth of the min max search depth
     *
     * @return
     */
    protected abstract int getMinMaxDepth();

    private class MiniMaxWorker implements Callable<Integer> {

        private final Board workerBoard;
        private final Node parentNode;
        private final PlayerMove parentMove;
        private final int initialDepth;
        private final boolean initialMaxPlayer;
        private final Heuristic heuristic;

        public MiniMaxWorker(Board workerBoard, Node parentNode, PlayerMove parentMove, int initialDepth,
                             boolean initialMaxPlayer, Heuristic heuristic) {
            this.workerBoard = workerBoard;
            this.parentNode = parentNode;
            this.parentMove = parentMove;
            this.initialDepth = initialDepth;
            this.initialMaxPlayer = initialMaxPlayer;
            this.heuristic = heuristic;
        }

        @Override
        public Integer call() throws Exception {
            int score = 0;

            Date startDate = new Date();
            //System.out.println("Worker for move" + parentMove + " started");

            //trace node
            Node sNode = new MinMaxTree.Node(parentNode, parentMove);
            parentNode.addChild(sNode);

            // do move
            workerBoard.doMove(parentMove);

            score = minimaxAlphaBeta(initialDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, initialMaxPlayer, parentNode);

            workerBoard.revertMove(parentMove);

            // trace node score
            sNode.setScore(score);

            long duration = (new Date().getTime() - startDate.getTime()) / 1000;
            //System.out.println(
            //        "Worker for move" + parentMove + " done. score: " + score + "; duration: " + duration + "s");

            return score;
        }

        /**
         * Minimax algorithm with alpha-beta pruning for faster tree search. results are traced in minmaxtree object.
         *
         * @param depth     max search depth
         * @param alpha     maximize result
         * @param beta      minize result
         * @param maxPlayer if its onw turn (true) or not (false)
         * @param node      minimaxtree node for tracing
         * @return score of this move / node
         */
        private int minimaxAlphaBeta(final int depth, int alpha, int beta, final boolean maxPlayer, Node node) {
            int score = 0;

            // get all posible moves to do for this turn.
            List<PlayerMove> nextMoves = workerBoard.getListOfNextMoves(maxPlayer);

            // reached max depth or game has ended
            if (nextMoves.isEmpty() || depth == 0 || workerBoard.gameSetteled()) {
                score = heuristic.getHeuristic(workerBoard);
            } else {
                // check all posible moves and compare thiere pay off scores
                for (PlayerMove move : nextMoves) {
                    // trace node
                    Node sNode = new MinMaxTree.Node(node, move);
                    node.addChild(sNode);

                    // do move
                    workerBoard.doMove(move);

                    if (maxPlayer) { //maxplayer are own turns
                        score = minimaxAlphaBeta(depth - 1, alpha, beta, false, sNode);
                        if (score > alpha) { //maximizing
                            alpha = score;
                        }
                    } else {
                        score = minimaxAlphaBeta(depth - 1, alpha, beta, true, sNode);
                        if (score < beta) { // minimizing
                            beta = score;
                        }
                    }

                    workerBoard.revertMove(move);

                    // trace node score
                    sNode.setScore(score);

                    // alpha / beta pruning
                    if (alpha >= beta) {
                        break;
                    }
                }

                // set score to max (alpha) or min (beta)
                score = (maxPlayer ? alpha : beta);
            }

            return score;
        }

    }
}
