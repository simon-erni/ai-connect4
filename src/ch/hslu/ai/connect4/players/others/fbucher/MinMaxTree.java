/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Florian Bucher
 */
public class MinMaxTree {

    private Node root;
    private static boolean enabled = true;

    public MinMaxTree(PlayerMove rootMove, boolean enabled) {
        root = new Node(null);
        root.move = rootMove;
        root.children = new ArrayList<>();
        this.enabled = enabled;
    }

    public Node getRoot() {
        return root;
    }

    public static class Node {

        private Integer score = null;
        private PlayerMove move;

        private Node parent;
        private volatile List<Node> children = new ArrayList<>();

        public Node(Node parent) {
            this.parent = parent;
        }

        public Node(Node parent, PlayerMove move) {
            this(parent);
            this.move = move;
        }

        private void print(String prefix, boolean isTail) {
            System.out.println(prefix + (isTail ? "└── " : "├── ") + "[" + score + " |" + move + "]");
            for (int i = 0; i < children.size() - 1; i++) {
                children.get(i).print(prefix + (isTail ? "    " : "│   "), false);
            }
            if (children.size() > 0) {
                children.get(children.size() - 1).print(prefix + (isTail ? "    " : "│   "), true);
            }
        }

        public void addChild(Node node) {
            if (enabled) {
                this.children.add(node);
            }
        }

        public void setScore(int score) {
            if (enabled) {
                this.score = score;
            }
        }

        private synchronized void clearChilds() {
            for (Node child : children) {
                child.clearChilds();
            }

            this.children.clear();
            this.children = null;

            this.score = null;
            this.move = null;
            this.parent = null;
        }
    }

    public void print() {
        if (enabled) {
            root.print("", true);
        }
    }

    public void clear() {
        PlayerMove m = root.move;
        Integer s = root.score;

        root.clearChilds();

        root.move = m;
        root.score = s;
        root.children = new ArrayList<>();
    }
}
