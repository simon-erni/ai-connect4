/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

/**
 * @author Florian Bucher
 */
public class PlayerAISecondImpRand extends PlayerAIBase {

    private final Heuristic heuristic = new RandomHeuristic();

    public PlayerAISecondImpRand(String name, char symbol) {
        super(name, symbol);
    }

    @Override
    protected Heuristic initHeuristic() {
        return heuristic;
    }

    @Override
    protected int getMinMaxDepth() {
        return 7;
    }

    private class RandomHeuristic implements Heuristic {

        @Override
        public int getHeuristic(Board board) {
            int hScore = 0;
            char gameWinner = board.gameSetteledBy();

            if (gameWinner == board.oppSymbol) {
                // worst possible move (must be better than MIN_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MIN_VALUE + 1;
            } else if (gameWinner == board.ownSymbol) {
                // best possible move (must be worse than MAX_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MAX_VALUE - 1;
            } else {
                hScore = (int) (Math.random() * 100 - 50);
            }

            return hScore;
        }
    }

}
