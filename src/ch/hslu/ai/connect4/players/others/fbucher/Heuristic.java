/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

/**
 * @author Florian Bucher
 */
public interface Heuristic {

    int getHeuristic(Board board);
}
