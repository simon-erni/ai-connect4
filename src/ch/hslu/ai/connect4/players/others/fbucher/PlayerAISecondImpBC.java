/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Florian Bucher
 */
public class PlayerAISecondImpBC extends PlayerAIBase {

    private final Heuristic heuristic = new PatternHeuristic();

    public PlayerAISecondImpBC(String name, char symbol) {
        super(name, symbol);
    }

    @Override
    protected Heuristic initHeuristic() {
        return heuristic;
    }

    @Override
    protected int getMinMaxDepth() {
        return 5;
    }

    public class PatternHeuristic implements Heuristic {

        @Override
        public int getHeuristic(Board board) {
            int hScore = 0;
            char gameWinner = board.gameSetteledBy();

            if (gameWinner == board.oppSymbol) {
                // worst possible move (must be better than MIN_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MIN_VALUE + 1;
            } else if (gameWinner == board.ownSymbol) {
                // best possible move (must be worse than MAX_VALUE, due to implementation von a/b-prunig)
                hScore = Integer.MAX_VALUE - 1;
            } else {
                hScore = calcBoard(board);
            }

            return hScore;
        }

        /**
         * Calculates the current board value based on patterns. !!Only Works with a fixed size board of 7 colums and
         * 6 rows!!
         *
         * @param board the board
         * @return the score of the analyzed board.
         */
        private int calcBoard(Board board) {
            // prepare list for checks
            ArrayList<String> cellGroupListH = new ArrayList<>();
            ArrayList<String> cellGroupListV = new ArrayList<>();
            HashMap<Integer, String> cellGroupListLR = new HashMap<>();
            HashMap<Integer, String> cellGroupListRL = new HashMap<>();

            // init values
            int row = 0;
            int col = 0;
            boolean diag5;
            boolean diag6;

            // get all cellGroups for each row / column / diagonal
            while (row < board.boardRows && col < board.boardCols) {
                //System.out.print("col:" + col + ",row:" + row);
                // check entire rows
                if (col == 0) {
                    cellGroupListH.add(""
                            + board.get(0, board.boardRows - 1 - row)
                            + board.get(1, board.boardRows - 1 - row)
                            + board.get(2, board.boardRows - 1 - row)
                            + board.get(3, board.boardRows - 1 - row)
                            + board.get(4, board.boardRows - 1 - row)
                            + board.get(5, board.boardRows - 1 - row)
                            + board.get(6, board.boardRows - 1 - row));
                }

                // check entire columns
                if (row == board.boardRows - 1) {
                    cellGroupListV.add(""
                            + board.get(col, 0)
                            + board.get(col, 1)
                            + board.get(col, 2)
                            + board.get(col, 3)
                            + board.get(col, 4)
                            + board.get(col, 5));
                }

                // check diagnoal
                if ((col == 0 && row > 2) || (col < 4 && row == board.boardRows - 1)) {
                    diag5 = (col == 0 && row > 3) || (col < 3 && row == board.boardRows - 1);
                    diag6 = (col < 2 && row == board.boardRows - 1);
                    // left-2-right
                    // index eg: c1, r5 : (1 + 5) - 3 = 3
                    cellGroupListLR.put(col + row - 3, ""
                            + board.get(col, row)
                            + board.get(col + 1, row - 1)
                            + board.get(col + 2, row - 2)
                            + board.get(col + 3, row - 3)
                            + (diag5 ? board.get(col + 4, row - 4) : '\0')
                            + (diag6 ? board.get(col + 5, row - 5) : '\0'));
                    // right-2-left
                    // index eg: c5, r5 : 5 + (5-5) - 3 = 2
                    cellGroupListRL.put(col + (row - board.boardRows + 1) - 3, ""
                            + board.get(board.boardCols - 1 - col, row)
                            + board.get(board.boardCols - 2 - col, row - 1)
                            + board.get(board.boardCols - 3 - col, row - 2)
                            + board.get(board.boardCols - 4 - col, row - 3)
                            + (diag5 ? board.get(board.boardCols - 5 - col, row - 4) : '\0')
                            + (diag6 ? board.get(board.boardCols - 6 - col, row - 5) : '\0'));
                }

                //System.out.println(" => " + cellGroupList.size());
                // change check position
                if (row < board.boardRows - 1) {
                    row++;
                } else {
                    col++;
                }

            }

            // compare cellGroups to move patterns
            int score = getPatternScoreHorizontal(cellGroupListH, board.emptySymbol, board.ownSymbol,
                    board.oppSymbol);
            score += getPatternScoreHorizontal(cellGroupListV, board.emptySymbol, board.ownSymbol,
                    board.oppSymbol);
            score += getPatternScoreDiagonalLR(cellGroupListLR, cellGroupListV, board.emptySymbol, board.ownSymbol,
                    board.oppSymbol);
            score += getPatternScoreDiagonalRL(cellGroupListRL, cellGroupListV, board.emptySymbol, board.ownSymbol,
                    board.oppSymbol);

            return score;
        }

        /**
         * Calculates the score for all horizontal cellGroups (rows).
         *
         * @param cellGroups the Cell groups started from the bottom one and fields from left to right
         * @param eSymb      the symbol for empty fields
         * @param owSymb     the symbol for own fields
         * @param opSymb     the symbol for opponent fields
         * @return the score for all cellGroups.
         */
        public int getPatternScoreHorizontal(ArrayList<String> cellGroups, char eSymb, char owSymb, char opSymb) {
            int score = 0;
            int indexOf = -1;
            String rowData;

            // important row 0 is the bottom row
            for (int row = 0; row < cellGroups.size(); row++) {
                rowData = cellGroups.get(row);
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // -xxx-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 5).contains("" + eSymb)) {
                        score += 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // -ooo-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 5).contains("" + eSymb)) {
                        score -= 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb)) >= 0) {  // -xxx
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb)) >= 0) {  // -ooo
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + owSymb)) >= 0) {  // x-xx
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + opSymb)) >= 0) {  // o-oo
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + owSymb)) >= 0) {  // xx-x
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + opSymb)) >= 0) {  // oo-o
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // xxx-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + eSymb)) >= 0) {   // -xx-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + eSymb)) >= 0) {   // -oo-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // ooo-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + eSymb)) >= 0) {   // xx--
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + eSymb)) >= 0) {   // oo--
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + owSymb)) >= 0) {   // --xx
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + opSymb)) >= 0) {   // --oo
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + owSymb)) >= 0) {   // x--x
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + opSymb)) >= 0) {   // o--o
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + owSymb)) >= 0) {   // -x-x
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + opSymb)) >= 0) {   // -o-o
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + eSymb)) >= 0) {   // x-x-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + eSymb)) >= 0) {   // o-o-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + eSymb)) >= 0) {    // -x--
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + eSymb)) >= 0) {    // -o--
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + eSymb)) >= 0) {    // --x-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + eSymb)) >= 0) {    // --o-
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + eSymb)) >= 0) {    // x---
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + eSymb)) >= 0) {    // o---
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + owSymb)) >= 0) {    // ---x
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + opSymb)) >= 0) {    // ---o
                    if (row < 1 || !cellGroups.get(row - 1).substring(indexOf, indexOf + 4).contains("" + eSymb)) {
                        score -= 1;
                    }
                }
            }
            return score;
        }

        /**
         * Calculates the score for all vertical cellGroups (columns).
         *
         * @param cellGroups the Cell groups started from the left to right and fields from bottom to top
         * @param eSymb      the symbol for empty fields
         * @param owSymb     the symbol for own fields
         * @param opSymb     the symbol for opponent fields
         * @return the score for all cellGroups.
         */
        public int getPatternScoreVertical(ArrayList<String> cellGroups, char eSymb, char owSymb, char opSymb) {
            int score = 0;

            for (String cellGroup : cellGroups) {
                if (cellGroup.contains("" + owSymb + owSymb + owSymb + eSymb)) {  // xxx-
                    score += 1009;
                }
                if (cellGroup.contains("" + opSymb + opSymb + opSymb + eSymb)) {  // ooo-
                    score -= 1009;
                }
                if (cellGroup.contains("" + owSymb + owSymb + eSymb + eSymb)) {   // xx--
                    score += 101;
                }
                if (cellGroup.contains("" + opSymb + opSymb + eSymb + eSymb)) {   // oo--
                    score -= 101;
                }
                if (cellGroup.contains("" + owSymb + eSymb + eSymb + eSymb)) {    // x---
                    score += 1;
                }
                if (cellGroup.contains("" + opSymb + eSymb + eSymb + eSymb)) {    // o---
                    score -= 1;
                }
            }
            return score;
        }

        /**
         * Calculates the score for all diagonal cellGroups (direction left to right).
         *
         * @param cellGroupsDiag the Cell groups diagonal started from the bottom left one to the top right
         * @param cellGroupsVert the Cell groups vertical started from left to right and from bottom up
         * @param eSymb          the symbol for empty fields
         * @param owSymb         the symbol for own fields
         * @param opSymb         the symbol for opponent fields
         * @return the score for all cellGroups.
         */
        public int getPatternScoreDiagonalLR(HashMap<Integer, String> cellGroupsDiag, ArrayList<String> cellGroupsVert,
                                             char eSymb, char owSymb, char opSymb) {
            int score = 0;
            int indexOf;
            int innerColI;
            int innerRowI;
            String rowData;

            // important row 0 is the bottom row
            for (Integer col : cellGroupsDiag.keySet()) {
                rowData = cellGroupsDiag.get(col);
                innerColI = (col < 2) ? 0 : col - 2;    //get series: 0 0 0 1 2 3
                innerRowI = (col >= 2) ? 0 : 2 - col;   //get series: 2 1 0 0 0 0

                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // -xxx-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 4).indexOf("" + eSymb) >= indexOf + innerRowI + 4) {
                        score += 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // -ooo-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 4).indexOf("" + eSymb) >= indexOf + innerRowI + 4) {
                        score -= 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb)) >= 0) {  // -xxx
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb)) >= 0) {  // -ooo
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + owSymb)) >= 0) {  // x-xx
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + opSymb)) >= 0) {  // o-oo
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + owSymb)) >= 0) {  // xx-x
                    if (cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + opSymb)) >= 0) {  // oo-o
                    if (cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // xxx-
                    if (cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // ooo-
                    if (cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + eSymb)) >= 0) {   // -xx-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + eSymb)) >= 0) {   // -oo-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + eSymb)) >= 0) {   // xx--
                    if (cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + eSymb)) >= 0) {   // oo--
                    if (cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + owSymb)) >= 0) {   // --xx
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + opSymb)) >= 0) {   // --oo
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + owSymb)) >= 0) {   // x--x
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + opSymb)) >= 0) {   // o--o
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + owSymb)) >= 0) {   // -x-x
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + opSymb)) >= 0) {   // -o-o
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + eSymb)) >= 0) {   // x-x-
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + eSymb)) >= 0) {   // o-o-
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + eSymb)) >= 0) {    // -x--
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + eSymb)) >= 0) {    // -o--
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + eSymb)) >= 0) {    // --x-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + eSymb)) >= 0) {    // --o-
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + eSymb)) >= 0) {    // x---
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + eSymb)) >= 0) {    // o---
                    if (cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI + indexOf + 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + owSymb)) >= 0) {    // ---x
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + opSymb)) >= 0) {    // ---o
                    if (cellGroupsVert.get(innerColI + indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI + indexOf + 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI + indexOf + 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 1;
                    }
                }
            }
            return score;
        }

        /**
         * Calculates the score for all diagonal cellGroups (direction right to left).
         *
         * @param cellGroupsDiag the Cell groups diagonal started from the bottom right one to the top left
         * @param cellGroupsVert the Cell groups vertical started from left to right and from bottom up.d
         * @param eSymb          the symbol for empty fields
         * @param owSymb         the symbol for own fields
         * @param opSymb         the symbol for opponent fields
         * @return the score for all cellGroups.
         */
        public int getPatternScoreDiagonalRL(HashMap<Integer, String> cellGroupsDiag, ArrayList<String> cellGroupsVert,
                                             char eSymb, char owSymb, char opSymb) {
            int score = 0;
            int indexOf;
            int innerColI;
            int innerRowI;
            int colCount = cellGroupsVert.size() - 1;
            String rowData;

            // important row 0 is the bottom row
            for (Integer col : cellGroupsDiag.keySet()) {
                rowData = cellGroupsDiag.get(col);
                innerColI = colCount - ((col < 2) ? 0 : col - 2);   //get series: 5 5 5 4 3 2
                innerRowI = (col >= 2) ? 0 : 2 - col;               //get series: 2 1 0 0 0 0

                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // -xxx-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 4).indexOf("" + eSymb) >= indexOf + innerRowI + 4) {
                        score += 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // -ooo-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 4).indexOf("" + eSymb) >= indexOf + innerRowI + 4) {
                        score -= 50021;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + owSymb)) >= 0) {  // -xxx
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + opSymb)) >= 0) {  // -ooo
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + owSymb)) >= 0) {  // x-xx
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + opSymb)) >= 0) {  // o-oo
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + owSymb)) >= 0) {  // xx-x
                    if (cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + opSymb)) >= 0) {  // oo-o
                    if (cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + owSymb + eSymb)) >= 0) {  // xxx-
                    if (cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + opSymb + eSymb)) >= 0) {  // ooo-
                    if (cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 1009;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + owSymb + eSymb)) >= 0) {   // -xx-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + opSymb + eSymb)) >= 0) {   // -oo-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 211;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + owSymb + eSymb + eSymb)) >= 0) {   // xx--
                    if (cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + opSymb + eSymb + eSymb)) >= 0) {   // oo--
                    if (cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + owSymb)) >= 0) {   // --xx
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + opSymb)) >= 0) {   // --oo
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + owSymb)) >= 0) {   // x--x
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + opSymb)) >= 0) {   // o--o
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + owSymb)) >= 0) {   // -x-x
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + opSymb)) >= 0) {   // -o-o
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + owSymb + eSymb)) >= 0) {   // x-x-
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + opSymb + eSymb)) >= 0) {   // o-o-
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 101;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + owSymb + eSymb + eSymb)) >= 0) {    // -x--
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + opSymb + eSymb + eSymb)) >= 0) {    // -o--
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + owSymb + eSymb)) >= 0) {    // --x-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + opSymb + eSymb)) >= 0) {    // --o-
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 2;
                    }
                }
                if ((indexOf = rowData.indexOf("" + owSymb + eSymb + eSymb + eSymb)) >= 0) {    // x---
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + opSymb + eSymb + eSymb + eSymb)) >= 0) {    // o---
                    if (cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2
                            && cellGroupsVert.get(innerColI - indexOf - 3).indexOf("" + eSymb) >= indexOf + innerRowI + 3) {
                        score -= 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + owSymb)) >= 0) {    // ---x
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score += 1;
                    }
                }
                if ((indexOf = rowData.indexOf("" + eSymb + eSymb + eSymb + opSymb)) >= 0) {    // ---o
                    if (cellGroupsVert.get(innerColI - indexOf).indexOf("" + eSymb) >= indexOf + innerRowI
                            && cellGroupsVert.get(innerColI - indexOf - 1).indexOf("" + eSymb) >= indexOf + innerRowI + 1
                            && cellGroupsVert.get(innerColI - indexOf - 2).indexOf("" + eSymb) >= indexOf + innerRowI + 2) {
                        score -= 1;
                    }
                }
            }
            return score;
        }
    }

}
