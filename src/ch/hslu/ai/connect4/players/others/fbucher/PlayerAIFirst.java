package ch.hslu.ai.connect4.players.others.fbucher;

import ch.hslu.ai.connect4.Player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Florian Bucher
 */
public class PlayerAIFirst extends Player {

    public static final char EMPTY_SYMBOL = '-';
    private char opponentSymbol;
    private char[][] minMaxBoard;
    private Stack<PlayerMove> moveStack = new Stack<>();

    public PlayerAIFirst(String name, char symbol) {
        super(name);
    }

    @Override
    public int play(char[][] board) {
        minMaxBoard = board.clone();
        opponentSymbol = getOpponentSymbol();

        PlayerMove bestMove = null;
        int payOff;
        int maxPayOff = Integer.MIN_VALUE;

        for (PlayerMove move : getPosibleMoves(true)) {
            payOff = limMinMaxAlphaBeta(move, 4, maxPayOff, Integer.MAX_VALUE, false);
            //payOff += isMoveNearCenter(move);
            //System.out.println(move + " has payOff of " + payOff);

            if (maxPayOff < payOff) {
                maxPayOff = payOff;
                bestMove = move;
            }
        }

        doPosibleMove(bestMove);

        return bestMove.col;
    }

    private int limMinMaxAlphaBeta(
            final PlayerMove rootMove, final int depth, int alpha,
            int beta, final boolean ownTurn) {
        int returnValue = 0;

        doPosibleMove(rootMove);

        boolean gameDone = gameDone();
        if (depth == 0 || gameDone) {
            returnValue = getHeuristic(rootMove, ownTurn);
        } else if (ownTurn) {
            for (PlayerMove subnode : getPosibleMoves(true)) {
                alpha = Math.max(alpha, limMinMaxAlphaBeta(subnode, depth - 1, alpha, beta, false));
                if (beta <= alpha) {
                    break;
                }
            }
            returnValue = alpha;
        } else {
            for (PlayerMove subnode : getPosibleMoves(false)) {
                beta = Math.min(beta, limMinMaxAlphaBeta(subnode, depth - 1, alpha, beta, true));
                if (beta <= alpha) {
                    break;
                }
            }
            returnValue = beta;
        }

        revertPosibleMove(rootMove);

        return returnValue;
    }

    private boolean gameDone() {
        // Check for a full board
        boolean done = true;
        for (int col = 0; col < minMaxBoard.length; col++) {
            done &= minMaxBoard[col][0] != EMPTY_SYMBOL;
        }
        if (done) {
//            System.out.print("Draw:");
//            printAllDoneMoves();
            return done;
        }

        // Check for a finished game
        int connectedOwn = 0;
        int connectedOpp = 0;
        int emptyCount = 0;

        // horizontal  4 connected
        for (int row = minMaxBoard[0].length - 1; row >= 0; row--) {
            for (int col = 0; col < minMaxBoard.length; col++) {
                if (minMaxBoard[col][row] == getSymbol()) {
                    connectedOpp = 0;
                    if ((++connectedOwn) == 4) {
//                        System.out.print("WON with horizontal:");
//                        printAllDoneMoves();
                        return true;
                    }
                } else if (minMaxBoard[col][row] == opponentSymbol) {
                    connectedOwn = 0;
                    if ((++connectedOpp) == 4) {
//                        System.out.print("LOST with horizontal:");
//                        printAllDoneMoves();
                        return true;
                    }
                } else {
                    emptyCount++;
                    connectedOwn = connectedOpp = 0;
                }
            }
            // if one row is all empty, upper rows are empty too.
            if (emptyCount == minMaxBoard.length - 1) {
                break;
            }
            connectedOwn = connectedOpp = emptyCount = 0;
        }

        connectedOwn = connectedOpp = emptyCount = 0;

        //Vertical 4 connected
        for (int col = 0; col < minMaxBoard.length; col++) {
            for (int row = minMaxBoard[0].length - 1; row >= 0; row--) {
                if (minMaxBoard[col][row] == getSymbol()) {
                    connectedOpp = 0;
                    if ((++connectedOwn) == 4) {
                        //System.out.print("WON with vertical:");
                        //printAllDoneMoves();
                        return true;
                    }
                } else if (minMaxBoard[col][row] == opponentSymbol) {
                    connectedOwn = 0;
                    if ((++connectedOpp) == 4) {
//                        System.out.print("LOST with vertical:");
//                        printAllDoneMoves();
                        return true;
                    }
                } else { //if one symbol is empty, upper symbols are empty too.
                    break;
                }
            }
            connectedOwn = connectedOpp = 0;
        }

        // diagonaly
        // Left-to-right diagonal:
        for (int col = 0; col <= minMaxBoard.length - 4; col++) {
            for (int row = 0; row <= minMaxBoard[0].length - 4; row++) {
                char[] cells = new char[]{
                        minMaxBoard[col][row], minMaxBoard[col + 1][row + 1],
                        minMaxBoard[col + 2][row + 2], minMaxBoard[col + 3][row + 3]};
                if (cells[0] != EMPTY_SYMBOL
                        && cells[0] == cells[1] && cells[1] == cells[2] && cells[2] == cells[3]) {
//                    System.out.printf("%s diagonal l-2-r:", cells[0] == getSymbol() ? "WON" : "LOST");
//                    printAllDoneMoves();
                    return true;
                }
            }
        }

        // Right-to-left diagonal:
        for (int column = minMaxBoard.length - 1; column >= 3; column--) {
            for (int row = 0; row <= minMaxBoard[0].length - 4; row++) {
                char[] cells = new char[]{
                        minMaxBoard[column][row], minMaxBoard[column - 1][row + 1],
                        minMaxBoard[column - 2][row + 2], minMaxBoard[column - 3][row + 3]};
                if (cells[0] != EMPTY_SYMBOL
                        && cells[0] == cells[1] && cells[1] == cells[2] && cells[2] == cells[3]) {
//                    System.out.printf("%s diagonal r-2-l:", cells[0] == getSymbol() ? "WON" : "LOST");
//                    printAllDoneMoves();
                    return true;
                }
            }
        }

        // all failed => game not done
        return false;
    }

    /**
     * @return
     */
    private char getOpponentSymbol() {
        for (int row = 0; row < minMaxBoard[0].length; row++) {
            for (int col = 0; col < minMaxBoard.length; col++) {
                if (minMaxBoard[col][row] != getSymbol()
                        && minMaxBoard[col][row] != EMPTY_SYMBOL) {
                    return minMaxBoard[col][row];
                }
            }
        }

        return getSymbol() != 'o' ? 'o' : 'x';
    }

    private List<PlayerMove> getPosibleMoves(boolean ownTurn) {
        List<PlayerMove> possibleNextColumn = new LinkedList<>();

        for (int col = 0; col < minMaxBoard.length; col++) {
            for (int row = minMaxBoard[0].length - 1; row >= 0; row--) {
                if (minMaxBoard[col][row] == EMPTY_SYMBOL) {
                    possibleNextColumn.add(new PlayerMove(col, row, ownTurn ? getSymbol() : opponentSymbol));
                    break;
                }
            }
        }

        return possibleNextColumn;
    }

    private void doPosibleMove(PlayerMove move) {
        moveStack.push(move);
        if (minMaxBoard[move.col][move.row] == EMPTY_SYMBOL) {
            minMaxBoard[move.col][move.row] = move.symbol;
            //System.out.println("Player '" + symbol + "' do move [" + move.x + "," + move.y + "]");
        } else {
            throw new RuntimeException("Invalid move! Field not empty!");
        }
    }

    private void revertPosibleMove(PlayerMove move) {
        PlayerMove revertedMove = moveStack.pop();
        if (minMaxBoard[move.col][move.row] == move.symbol && revertedMove == move) {
            minMaxBoard[move.col][move.row] = EMPTY_SYMBOL;
        } else if (revertedMove != move) {
            throw new RuntimeException("Wrong move revered! " + move);
        } else {
            throw new RuntimeException("Invalid move to revert! Ownership wrong! " + move);
        }
    }

    private int getHeuristic(PlayerMove lastMove, boolean ownTurn) {
        HeuristicObject ownConnected = new HeuristicObject();
        HeuristicObject oppConnected = new HeuristicObject();

        // Check for a finished game
        int ownConCount = 0;
        int oppConCount = 0;
        int emptyCount = 0;

        //printMinMaxBoard();
        // Horizontal Down-to-Up 4 connected
        for (int row = minMaxBoard[0].length - 1; row >= 0; row--) {
            for (int col = 0; col < minMaxBoard.length; col++) {
                if (oppConCount + ownConCount > 4) { //if there are 4 connected, game is done
                    break;
                } else if (minMaxBoard[col][row] == getSymbol()) {
                    oppConCount = 0; //reset opponent
                    ownConnected.updateScore(HeuristicCheckDirection.BottomUp, new PlayerMove(col, row, getSymbol()),
                            ++ownConCount);
                } else if (minMaxBoard[col][row] == opponentSymbol) {
                    ownConCount = 0; //reset opponent
                    oppConnected.updateScore(HeuristicCheckDirection.BottomUp,
                            new PlayerMove(col, row, opponentSymbol), ++oppConCount);
                } else { //emtpy symbol brakes both
                    emptyCount++;
                    ownConCount = oppConCount = 0;
                }
            }
            // if full row is empty, the upper rows are emtpy too.
            if (emptyCount == minMaxBoard[0].length) {
                break;
            }
            ownConCount = oppConCount = emptyCount = 0;
        }

        ownConCount = oppConCount = emptyCount = 0;
        // Vertiocal left to Right 4 connected
        for (int col = 0; col < minMaxBoard.length; col++) {
            for (int row = minMaxBoard[0].length - 1; row >= 0; row--) {
                if (oppConCount + ownConCount > 4) { //if there are 4 connected, game is done
                    break;
                } else if (minMaxBoard[col][row] == getSymbol()) {
                    oppConCount = 0; //reset opponent
                    ownConnected.updateScore(HeuristicCheckDirection.LeftRight, new PlayerMove(col, row, getSymbol()),
                            ++ownConCount);
                } else if (minMaxBoard[col][row] == opponentSymbol) {
                    ownConCount = 0; //reset opponent
                    oppConnected.updateScore(HeuristicCheckDirection.LeftRight,
                            new PlayerMove(col, row, opponentSymbol), ++oppConCount);
                } else { //when empty upperfields are empty too.
                    break;
                }
            }
            ownConCount = oppConCount = 0;
        }

        // Left-down-to-right-up diagonal 4 connected:
        // Start on 2nd upper left field.
        int col = 0;
        int row = 3;
        while (row < minMaxBoard[0].length && col < minMaxBoard.length - 4) {
            for (int cellX = col, cellY = row;
                 cellX < minMaxBoard.length && cellY >= 0;
                 cellX++, cellY--) {
                if (oppConCount + ownConCount > 4) { //if there are 4 connected, game is done
                    break;
                } else if (minMaxBoard[cellX][cellY] == getSymbol()) {
                    oppConCount = 0; //reset opponent
                    ownConnected.updateScore(HeuristicCheckDirection.DiagonalBottomLeftRightUp,
                            new PlayerMove(col, row, getSymbol()), ++ownConCount);
                } else if (minMaxBoard[cellX][cellY] == opponentSymbol) {
                    ownConCount = 0; //reset opponent
                    oppConnected.updateScore(HeuristicCheckDirection.DiagonalBottomLeftRightUp,
                            new PlayerMove(col, row, opponentSymbol), ++oppConCount);
                } else { //emtpy symbol brakes both
                    ownConCount = oppConCount = 0;
                }
            }
            ownConCount = oppConCount = 0;

            if (row != minMaxBoard[0].length - 1) {
                row++;
            } else {
                col++;
            }
        }

        // right-down-to-left-up diagonal 4 connected:
        // Start on 2nd upper right field.
        col = minMaxBoard.length - 1;
        row = 3;
        while (row < minMaxBoard[0].length && col > 3) {
            for (int cellX = col, cellY = row;
                 cellX >= 0 && cellY >= 0;
                 cellX--, cellY--) {
                if (oppConCount + ownConCount > 4) { //if there are 4 connected, game is done
                    break;
                } else if (minMaxBoard[cellX][cellY] == getSymbol()) {
                    oppConCount = 0; //reset opponent
                    ownConnected.updateScore(HeuristicCheckDirection.DiagnoalBottomRightLeftUp,
                            new PlayerMove(col, row, getSymbol()), ++ownConCount);
                } else if (minMaxBoard[cellX][cellY] == opponentSymbol) {
                    ownConCount = 0; //reset opponent
                    oppConnected.updateScore(HeuristicCheckDirection.DiagnoalBottomRightLeftUp,
                            new PlayerMove(col, row, opponentSymbol), ++oppConCount);
                } else { //emtpy symbol brakes both
                    ownConCount = oppConCount = 0;
                }
            }
            ownConCount = oppConCount = 0;

            if (row != minMaxBoard[0].length - 1) {
                row++;
            } else {
                col--;
            }
        }

        int ownHeuristic = ownConnected.calculateHeuristic() * (ownTurn ? 1 : 1);
        int oppHeuristic = oppConnected.calculateHeuristic() * (ownTurn ? 1 : 1);
        int heuristic = ownHeuristic - oppHeuristic;
        //System.out.printf("Heuristic for move[%d,%d] is: %d, [Own: %d, Opp: %d]\n", lastMove.x, lastMove.y, heuristic,
        //       ownConnected.calculateHeuristic(),
        //       oppConnected.calculateHeuristic());
        return heuristic;
    }

    public int isMoveNearCenter(PlayerMove move) {
        int value = Math.round(minMaxBoard.length / 2) - Math.abs(Math.round(move.col - minMaxBoard.length / 2));
        //System.out.println("near center value: " + value + " for move " + move);
        return value;
    }

    private void printMinMaxBoard() {
        for (int row = 0; row < minMaxBoard[0].length; row++) {
            for (int col = 0; col < minMaxBoard.length; col++) {
                System.out.print("[" + minMaxBoard[col][row] + "]");
            }
            System.out.println("");
        }
        System.out.print("");
    }

    private void printAllDoneMoves() {
        for (PlayerMove move : moveStack) {
            System.out.print("=>" + move);
        }
        System.out.println();
    }

    /**
     * Stores the heuristic data.
     *
     * @author Florian Bucher
     */
    private class HeuristicObject extends HashMap<Integer, Integer> {

        /**
         * Updates the Score for the heuristic object.
         *
         * @param checkDirection the direction its searched
         * @param move           the move which is checked
         * @param connectedCount how many stones are connected.
         */
        public void updateScore(HeuristicCheckDirection checkDirection, PlayerMove move, int connectedCount) {
            if (connectedCount > 1) {
                put(connectedCount, containsKey(connectedCount) ? get(connectedCount) + 1 : 1);
                if (containsKey(connectedCount - 1)) {
                    put(connectedCount - 1, get(connectedCount - 1) - 1);
                }

                // remove 3 and 2 connect which can't become a 4 connect.
                // eg: (bottomrow) [o][x][o][o][x][x][x] is 3 connect but never will be 4 connect.
                if (connectedCount < 4) {
                    if (checkDirection == HeuristicCheckDirection.BottomUp && move.row == 0) {
                        put(connectedCount, get(connectedCount) - 1);
                    } else if (checkDirection == HeuristicCheckDirection.LeftRight
                            && move.col == minMaxBoard[0].length - 1) {
                        put(connectedCount, get(connectedCount) - 1);
                    } else if (checkDirection == HeuristicCheckDirection.DiagonalBottomLeftRightUp
                            && (move.col == minMaxBoard[0].length - 1 || move.row == 0)) {
                        put(connectedCount, get(connectedCount) - 1);
                    } else if (checkDirection == HeuristicCheckDirection.DiagonalBottomLeftRightUp
                            && (move.col == 0 || move.row == 0)) {
                        put(connectedCount, get(connectedCount) - 1);
                    }
                }
            }
        }

        /**
         * Calculates the heuristic Value based on the score list. Uses the formula for heuristic: ( 2 ^
         * (count_of_connected_stones - 1) - 1) ^ 10. EG. 4 connected : ((2 ^ 3) - 1) ^ 10 = 2824751249
         *
         * @return the heuristic value.
         */
        public int calculateHeuristic() {
            int heuristic = 0;
            for (Integer connectCout : keySet()) {
                heuristic += Math.pow(Math.pow(2, connectCout - 1) - 1, 10);
            }
            return heuristic;
        }
    }
}
