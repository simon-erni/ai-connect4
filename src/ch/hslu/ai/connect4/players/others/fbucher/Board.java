/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Holds the game board and information about it.
 *
 * @author Florian Bucher
 */
public class Board {

    private static int oc = 0;
    private int ocn;

    private final char[][] board;
    private final Stack<PlayerMove> storedMoves;

    public final char ownSymbol;
    public final char oppSymbol;
    public final char emptySymbol = '-';
    public final int boardCols;
    public final int boardRows;
    public int oppMoveCount = 0;
    public int ownMoveCount = 0;

    /**
     * Initialize a Board for the ai. Gets all information about the board an the players and copies the board into
     * the internal store.
     *
     * @param board     the board to be copied
     * @param ownSymbol the current player symbol.
     */
    public Board(char[][] board, char ownSymbol) {
        this.boardCols = board.length;
        this.boardRows = board[0].length;
        this.board = new char[this.boardCols][this.boardRows];
        this.ownSymbol = ownSymbol;

        // defina a possible opponent symbol that differents from player symbol
        char possibleOppSymbol;
        if (ownSymbol != 'o') {
            possibleOppSymbol = 'o';
        } else {
            possibleOppSymbol = '0';
        }
        // copy field and find oppnent symbol
        for (int col = 0; col < board.length; col++) {
            for (int row = board[0].length - 1; row >= 0; row--) {
                char symbol = this.board[col][row] = board[col][row];
                if (symbol == ownSymbol) {
                    ownMoveCount++;
                } else if (symbol != emptySymbol) {
                    oppMoveCount++;
                    possibleOppSymbol = symbol;
                }
            }
        }

        this.oppSymbol = possibleOppSymbol;
        this.storedMoves = new Stack<>();

        ocn = ++oc;
    }

    public char get(int col, int row) {
        return board[col][row];
    }

    /**
     * Does a move on the board.
     *
     * @param move specified move.
     * @throws RuntimeException when move already done.
     */
    public void doMove(PlayerMove move) {
        if (storedMoves.contains(move)) {
            throw new RuntimeException("Invalid move: already done!!");
        } else if (board[move.col][move.row] == oppSymbol) {
            throw new RuntimeException("Invalid move: already done by other player!!");
        } else {
            storedMoves.push(move);
            board[move.col][move.row] = move.symbol;

            if (move.symbol == ownSymbol) {
                ownMoveCount++;
            } else if (move.symbol == oppSymbol) {
                oppMoveCount++;
            }
        }
    }

    /**
     * Reverts done move on the board
     *
     * @param move specified move.
     * @throws RuntimeException when move never done or not first on stack.
     */
    public void revertMove(PlayerMove move) {
        if (move != storedMoves.peek()) {
            throw new RuntimeException("Invalid revert: Move not first in stack!! ");
        } else if (!storedMoves.contains(move)) {
            throw new RuntimeException("Invalid revert: Move never done!!");
        } else {
            storedMoves.pop();
            board[move.col][move.row] = emptySymbol;

            if (move.symbol == ownSymbol) {
                ownMoveCount--;
            } else if (move.symbol == oppSymbol) {
                oppMoveCount--;
            }
        }
    }

    /**
     * Searches board for all possible next moves and returns a list of this moves.
     *
     * @param ownTurn defines if moves are for this player or opponent.
     * @return list of valid moves.
     */
    public List<PlayerMove> getListOfNextMoves(final boolean ownTurn) {
        List<PlayerMove> moves = new ArrayList<>();
        char symbol = ownTurn ? ownSymbol : oppSymbol;

        int divisor = 2;
        int colNum = 0;
        for (int col = 0; col < boardCols; col++, divisor *= -1) {
            for (int row = boardRows - 1; row >= 0; row--) {
                colNum = (int) (boardCols / 2) - (int) ((col + 1) / divisor);
                if (board[colNum][row] == emptySymbol) {
                    moves.add(new PlayerMove(colNum, row, symbol));
                    break;
                }
            }
        }

        return moves;
    }

    /**
     * Check if a game is settled. means one of the player has won the game.
     *
     * @return true when one player has 4 connected, otherwise false (draw count as false).
     */
    public boolean gameSetteled() {
        return gameSetteledBy() != emptySymbol;
    }

    /**
     * Check if a game is settled. means one of the player has won the game.
     *
     * @return returns the player symbol which has won. if none has won or it' draw the empty symbol is returned.
     */
    public char gameSetteledBy() {
        // prepare list for checks
        char[][] connectCellCheckList = {{}, {}, {}, {}};

        // check all fields and look for connect 4s
        for (int col = 0; col < boardCols; col++) {
            for (int row = boardRows - 1; row >= 0; row--) {
                if (row > 2) {
                    connectCellCheckList[0] = new char[]{
                            board[col][row], board[col][row - 1], board[col][row - 2], board[col][row - 3]};
                }
                if (col < boardCols - 3) {
                    connectCellCheckList[1] = new char[]{
                            board[col][row], board[col + 1][row], board[col + 2][row], board[col + 3][row]};
                }
                if (col > 2 && row > 2) {
                    connectCellCheckList[2] = new char[]{
                            board[col][row], board[col - 1][row - 1], board[col - 2][row - 2], board[col - 3][row - 3]};
                }
                if (col < boardCols - 3 && row > 2) {
                    connectCellCheckList[3] = new char[]{
                            board[col][row], board[col + 1][row - 1], board[col + 2][row - 2], board[col + 3][row - 3]};
                }

                for (char[] cells : connectCellCheckList) {
                    if (cells.length == 4 && cells[0] != emptySymbol
                            && cells[0] == cells[1] && cells[1] == cells[2] && cells[2] == cells[3]) {
                        return cells[0];
                    }
                }
            }
        }

        return emptySymbol;
    }

    /**
     * get a new clone from this board
     *
     * @return new board objected with cloand board data.
     */
    public Board getClone() {
        Board clone = new Board(board, ownSymbol);
        return clone;
    }
}
