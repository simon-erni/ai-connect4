/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

/**
 * Defines the Direction in which is searched.
 *
 * @author Florian Bucher
 */
public enum HeuristicCheckDirection {

    BottomUp,
    LeftRight,
    DiagonalBottomLeftRightUp,
    DiagnoalBottomRightLeftUp
}
