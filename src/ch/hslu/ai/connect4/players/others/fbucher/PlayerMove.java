/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.ai.connect4.players.others.fbucher;

/**
 * @author Florian Bucher
 */
public class PlayerMove {

    public int row;
    public int col;
    public char symbol;

    public PlayerMove(final int col, final int row, final char symbol) {
        this.row = row;
        this.col = col;
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return String.format("['%c',%d,%d]", symbol, col, row);
    }
}
