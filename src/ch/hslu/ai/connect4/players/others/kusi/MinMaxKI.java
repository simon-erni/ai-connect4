package ch.hslu.ai.connect4.players.others.kusi;

import ch.hslu.ai.connect4.Game;
import ch.hslu.ai.connect4.Player;

import java.io.IOException;
import java.io.Serializable;
import java.util.EventListener;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class MinMaxKI implements Serializable {
    private Game game;
    private int level = 7;
    private Player tPlayer;
    private transient ExecutorService executor;
    private boolean makeThread = true;

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        executor = Executors.newFixedThreadPool(game.getColumns() + 1);
    }

    public void bind(Game game) {
        this.game = game;
        executor = Executors.newFixedThreadPool(game.getColumns() + 1);
    }

    public int suggestCol(EventListener e) {
        this.tPlayer = game.getPlayer1();
        try {
            return getMax(executor.submit(new MinMax(game, level)).get());
        } catch (Exception ex) {
            throw new RuntimeException("Thread Died");
        }
    }

    private int getMax(float[] minmax) {
        int firstAllowed = 0;
        while (!game.isColumnFull(firstAllowed, game.getPlayer1())) {
            firstAllowed++;
        }

        int maxKey = firstAllowed;
        float maxValue = minmax[firstAllowed];
        for (int i = firstAllowed + 1; i < minmax.length; i++) {
            if (game.isColumnFull(i, game.getPlayer1()) && maxValue < minmax[i]) {
                maxValue = minmax[i];
                maxKey = i;
            }
        }
        return maxKey;
    }

    private class MinMax implements Callable<float[]> {
        private Game game;
        private int level;
        private HashMap<Integer, Future<float[]>> tasks;
        private float[] results;

        public MinMax(Game game, int level) {
            this.game = game;
            this.level = level;
            this.tasks = new HashMap<>();
            this.results = new float[game.getColumns()];
        }

        @Override
        public float[] call() {
//            for(int col = 0; col < game.getColumns(); col++) {
//                if(!game.isColumnFull(col, game.getPlayer1())) {
//                    continue;
//                }
//
//                if(game.hasWon(game.getPlayer1())) {
//                    float minMax = (game.getPlayer1().isSameTeam(tPlayer)) ? 1 : -2;
//                    results[col] = (float)(minMax * Math.pow(game.getColumns(), level));
//                    for(int j = 0; j < col; j++) {
//                        if(results[col] == results[j]) {
//                            for(int k = 0;k < col; k++) {
//                                results[k] = 0;
//                            }
//                            results[col] = (float)(minMax * Math.pow(game.getColumns(), level + 1));
//                            results[j] = 0;
//                            return results;
//                        }
//                    }
//                } else if (level > 0) {
//                    try {
//                        Game cGame = (Game) game.clone();
//                        cGame.addDisc(col);
//                        MinMax minmax = new MinMax(cGame, level -1);
//                        
//                        if(makeThread) {
//                            tasks.put(col, executor.submit(minmax));
//                            makeThread = false;
//                        } else {
//                            results[col] = calcSum(minmax.call());
//                        }
//                    } catch(Exception e) {
//                        System.out.println("Not reached: " + col);
//                    }
//                }   
//            }
//            try {
//                for(int col: tasks.keySet()) {
//                    results[col] = calcSum(tasks.get(col).get());
//                }
//            } catch(Exception e) {
//                throw new RuntimeException("Thread Died");
//            }
//            
//            return results;
//        }
//        
//        private float calcSum(float[] tempResults) 
//        {
//            float tmp = 0;
//            for(float i: tempResults) {
//                tmp += i;
//            }
//            return tmp;

            //fake return
            return null;
        }
    }
}
