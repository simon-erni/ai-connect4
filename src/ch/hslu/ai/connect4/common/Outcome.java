package ch.hslu.ai.connect4.common;


public enum Outcome {
    NOT_FINISHED,
    I_WIN,
    ENEMY_WINS,
    DRAW
}
