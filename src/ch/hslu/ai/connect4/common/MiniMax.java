package ch.hslu.ai.connect4.common;

import java.util.List;

public class MiniMax {

    private Heuristic heuristic;

    protected void setHeuristic(Heuristic heuristic) {
        this.heuristic = heuristic;
    }

    /**
     * Returns a column for the next move.
     *
     * @param heuristic The heuristic for evaluating positions.
     * @param board     The current board.
     * @param maxDepth  The max depth where it should search.
     * @return The column.
     */
    public int getMove(Heuristic heuristic, Board board, int maxDepth) {

        this.heuristic = heuristic;

        List<Integer> possibleMoves = board.getListOfNextMoves();

        int bestScore = Integer.MIN_VALUE;
        int bestMove = -1;


        for (int move : possibleMoves) {
            board.makeMove(move);
            int score = miniMax(maxDepth - 1, board);
            board.revertMove(move);


            if (score >= bestScore) {
                bestScore = score;
                bestMove = move;
            }
        }

        if (bestMove == -1) {
            bestMove = possibleMoves.get(0);
        }
        return bestMove;
    }


    /**
     * Returns the score for this board.
     *
     * @param maxDepth The max search depth.
     * @param board    The board.
     * @return score.
     */
    protected int miniMax(int maxDepth, Board board) {
        List<Integer> possibleMoves = board.getListOfNextMoves();

        if (maxDepth == 0 || possibleMoves.isEmpty()) {
            return heuristic.rateBoard(board);
        }

        int bestScore;

        if (board.isMyTurn()) {
            bestScore = Integer.MIN_VALUE;
        } else {
            bestScore = Integer.MAX_VALUE;
        }

        for (int move : possibleMoves) {

            board.makeMove(move);
            int score = miniMax(maxDepth - 1, board);
            board.revertMove(move);

            if (board.isMyTurn()) {
                if (score > bestScore) {
                    bestScore = score;
                }
            } else {
                if (score < bestScore) {
                    bestScore = score;
                }
            }
        }
        return bestScore;
    }

}
