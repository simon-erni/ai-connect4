package ch.hslu.ai.connect4.common;

import com.sun.istack.internal.NotNull;


public class MiniMaxResult implements Comparable<MiniMaxResult> {


    private int move;
    private int score;

    public MiniMaxResult(int move, int score) {
        this.move = move;
        this.score = score;
    }

    public int getMove() {
        return move;
    }


    public void setMove(int move) {
        this.move = move;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    @Override
    public int compareTo(@NotNull MiniMaxResult other) {

        return other.getScore() - this.getScore();

    }


}
