package ch.hslu.ai.connect4.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {

    private static final char EMPTY_SYMBOL = '-';

    private char opponentSymbol;

    private char[][] fields;

    private final int width;
    private final int height;

    private final char mySymbol;

    private int lastMove = -1;

    private boolean myTurn;

    /**
     * Generates an empty Board with specified width and height.
     *
     * @param width    The width of the generated board.
     * @param height   The height of the generated board.
     * @param mySymbol The symbol of the AI Player.
     * @param myTurn   specifies if I started the game.
     */
    public Board(int width, int height, char mySymbol, char opponentSymbol, boolean myTurn) {
        this.width = width;
        this.height = height;
        this.mySymbol = mySymbol;
        this.myTurn = myTurn;

        this.fields = new char[width][height];

        for (int i = 0; i < width; i++) {
            for (int k = 0; k < height; k++) {
                fields[i][k] = EMPTY_SYMBOL;
            }
        }

        this.opponentSymbol = opponentSymbol;

    }

    /**
     * Creates a board with predefined stack of moves and fields. The fields are going to be copied and inverted (upside down).
     *
     * @param fields   The fields of the board.
     * @param mySymbol The symbol of my Player.
     * @param myTurn   Specifies if it's currently my turn.
     */
    public Board(char[][] fields, char mySymbol, boolean myTurn) {
        this.width = fields.length;

        if (width > 0) {
            this.height = fields[0].length;
        } else {
            this.height = 0;
        }


        this.fields = new char[width][height];

        for (int i = 0; i < width; i++) {
            for (int k = 0; k < height; k++) {
                this.fields[i][k] = fields[i][height - k - 1];
            }
        }

        this.mySymbol = mySymbol;

        // defina a possible opponent symbol that differents from player symbol
        char possibleOppSymbol;
        if (mySymbol != 'o') {
            possibleOppSymbol = 'o';
        } else {
            possibleOppSymbol = '0';
        }
        // copy field and find oppnent symbol
        for (int col = 0; col < this.fields.length; col++) {
            for (int row = this.fields[0].length - 1; row >= 0; row--) {
                char symbol = this.fields[col][row] = this.fields[col][row];

                if (symbol != EMPTY_SYMBOL && symbol != mySymbol) {
                    possibleOppSymbol = symbol;
                }
            }
        }

        this.opponentSymbol = possibleOppSymbol;

        this.myTurn = myTurn;
    }


    /**
     * Inserts a Move in a Board.
     *
     * @param column Column where the move should be injected
     */
    public void makeMove(int column) throws IllegalArgumentException {

        char symbol = getSymbolOfCurrentPlayer();

        if (column < 0 || column >= width) {
            throw new IllegalArgumentException("Invalid Column Number");
        }

        if (isOccupied(fields[column][height - 1])) {
            throw new IllegalArgumentException("Column is full");
        }

        int row = 0;
        while (isOccupied(fields[column][row]) && row < height) {
            row++;
        }

        fields[column][row] = symbol;

        this.myTurn = !myTurn;

        this.lastMove = column;

    }

    /**
     * Removes a Move from a Board.
     *
     * @param column Column where the move should be removed.
     */
    public void revertMove(int column) throws IllegalArgumentException {

        if (column < 0 || column >= width) {
            throw new IllegalArgumentException("Invalid Column Number");
        }

        if (isFree(fields[column][0])) {
            throw new IllegalArgumentException("Column is empty");
        }

        int row = height - 1;
        while (isFree(fields[column][row]) && row >= 0) {
            row--;
        }

        fields[column][row] = EMPTY_SYMBOL;

        this.myTurn = !myTurn;

        this.lastMove = -1;

    }

    /**
     * Returns the fields for efficient programming.
     *
     * @return The fields.
     */
    public char[][] getFields() {
        return fields;
    }

    private char getSymbolOfCurrentPlayer() {
        if (myTurn) {
            return mySymbol;
        } else {
            return opponentSymbol;

        }
    }

    /**
     * Checks if the value means the field is occupied.
     *
     * @param value Value of the field
     * @return True if it's occupied, false if it's free.
     */
    private boolean isOccupied(char value) {
        return value != EMPTY_SYMBOL;
    }

    /**
     * Checks if the Value means that the field is free.
     *
     * @param value The value of the field.
     * @return True if its free, false if it's not.
     */
    private boolean isFree(char value) {
        return !isOccupied(value);
    }

    /**
     * Returns a String representation of the Board.
     *
     * @return The Board as String.
     */
    @Override
    public String toString() {
        String out = "";

        for (int i = 0; i < width + 2; i++) {
            out += "#";
        }
        out += "\n";

        for (int k = height - 1; k >= 0; k--) {
            out += "#";
            for (int i = 0; i < width; i++) {
                out += fields[i][k];
            }
            out += "#\n";
        }


        for (int i = 0; i < width + 2; i++) {
            out += "#";
        }
        out += "\n\n";

        return out;

    }

    /**
     * Checks if the game is draw by looking at all the top columns. If they are all full, then the game has ended.
     *
     * @return True: it's full. False: it's not full.
     */
    private boolean isDraw() {
        for (int i = 0; i < width; i++) {
            if (fields[i][height - 1] == EMPTY_SYMBOL) {
                return false;
            }
        }
        return true;

    }

    /**
     * Returns the detailed outcome.
     *
     * @return an Outcome enum.
     */
    public Outcome getOutcome() {
        char symbol = getWinnerSymbol();

        if (symbol == mySymbol) {
            return Outcome.I_WIN;
        } else if (symbol != EMPTY_SYMBOL) {
            return Outcome.ENEMY_WINS;
        } else {
            if (isDraw()) {
                return Outcome.DRAW;
            } else {
                return Outcome.NOT_FINISHED;
            }
        }
    }

    /**
     * Returns the symbol of the player that won.
     *
     * @return the symbol.
     */
    private char getWinnerSymbol() {
        char[][] connectedCells = {{}, {}, {}, {}};

        for (int col = 0; col < width; col++) {
            for (int row = height - 1; row >= 0; row--) {
                if (row > 2) {
                    connectedCells[0] = new char[]{
                            fields[col][row], fields[col][row - 1], fields[col][row - 2], fields[col][row - 3]};
                }
                if (col < width - 3) {
                    connectedCells[1] = new char[]{
                            fields[col][row], fields[col + 1][row], fields[col + 2][row], fields[col + 3][row]};
                }
                if (col > 2 && row > 2) {
                    connectedCells[2] = new char[]{
                            fields[col][row], fields[col - 1][row - 1], fields[col - 2][row - 2], fields[col - 3][row - 3]};
                }
                if (col < width - 3 && row > 2) {
                    connectedCells[3] = new char[]{
                            fields[col][row], fields[col + 1][row - 1], fields[col + 2][row - 2], fields[col + 3][row - 3]};
                }

                for (char[] cells : connectedCells) {
                    if (cells.length == 4 && cells[0] != EMPTY_SYMBOL
                            && cells[0] == cells[1] && cells[1] == cells[2] && cells[2] == cells[3]) {
                        return cells[0];
                    }
                }
            }
        }

        return EMPTY_SYMBOL;
    }

    /**
     * Returns the current player.
     *
     * @return true: if it's my turn. false: if it's not my turn.
     */
    public boolean isMyTurn() {
        return myTurn;
    }

    /**
     * Returns a list of possible next moves.
     *
     * @return a List of possible next moves.
     */
    public List<Integer> getListOfNextMoves() {

        List<Integer> list = new ArrayList<>();

        if (getOutcome() != Outcome.NOT_FINISHED) {
            return list;
        }

        for (int i = 0; i < width; i++) {
            if (isFree(fields[i][height - 1])) {
                list.add(i);
            }
        }

        return list;
    }

    /**
     * Returns the last move. The history only goes back 1 step. Afterwards, it is -1.
     *
     * @return A positive integer for the column. If a negative integer is returned, the last move could not be determined.
     */
    public int getLastMove() {
        return this.lastMove;
    }

    /**
     * Returns the empty symbol.
     *
     * @return a char.
     */
    public char getEmptySymbol() {
        return EMPTY_SYMBOL;
    }

    public char getOpponentSymbol() {
        return opponentSymbol;
    }

    public char getMySymbol() {
        return mySymbol;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Checks if the specified move is possibru.
     *
     * @param column The column
     * @param row    The inden^ded row.
     * @return If it's possibru or impossibru!!!
     */
    public boolean isMovePossible(int column, int row) {


        if (fields[column][row] != EMPTY_SYMBOL) {
            return false;
        }


        try {
            this.makeMove(column);
        } catch (IllegalArgumentException ex) {
            return false;
        }

        boolean possible = fields[column][row] != EMPTY_SYMBOL;

        this.revertMove(column);

        return possible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        if (width != board.width) return false;
        if (height != board.height) return false;
        if (mySymbol != board.mySymbol) return false;
        if (myTurn != board.myTurn) return false;
        return Arrays.deepEquals(fields, board.fields);

    }

}
