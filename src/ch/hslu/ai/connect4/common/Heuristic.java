package ch.hslu.ai.connect4.common;


public interface Heuristic {
    /**
     * Rates the current State of the board.
     *
     * @param board the board
     * @return The rating.
     */
    int rateBoard(Board board);
}
