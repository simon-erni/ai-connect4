package ch.hslu.ai.connect4.heuristic;

import ch.hslu.ai.connect4.common.Board;
import ch.hslu.ai.connect4.common.Heuristic;
import ch.hslu.ai.connect4.common.Outcome;

public class RandomHeuristic implements Heuristic {


    @Override
    public int rateBoard(Board board) {

        Outcome outcome = board.getOutcome();

        switch (outcome) {
            case DRAW:
                return 0;

            case ENEMY_WINS:
                return Integer.MIN_VALUE + 1;

            case I_WIN:
                return Integer.MAX_VALUE - 1;

            case NOT_FINISHED:
            default:
                return 0;
        }
    }
}
