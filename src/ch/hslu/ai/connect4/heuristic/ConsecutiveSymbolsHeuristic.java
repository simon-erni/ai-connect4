package ch.hslu.ai.connect4.heuristic;

import ch.hslu.ai.connect4.common.Board;
import ch.hslu.ai.connect4.common.Heuristic;
import ch.hslu.ai.connect4.common.Outcome;

/**
 * Diese Heuristik bewertet die Felder (Spielzüge) anhand der
 * beieinanderliegenden Spielstein auf dem Feld.
 *
 * @author Stefan
 */
public class ConsecutiveSymbolsHeuristic implements Heuristic {

    private int myMaxCounter = 0;
    private int opponentMaxCounter = 0;

    private int myCounter = 0;
    private int opponentCounter = 0;

    private Board board;

    @Override
    public final int rateBoard(final Board board) {

        this.board = board;

        final Outcome outcome = board.getOutcome();

        switch (outcome) {
            case DRAW:
                return 0;

            case ENEMY_WINS:
                return Integer.MIN_VALUE + 1;

            case I_WIN:
                return Integer.MAX_VALUE - 1;

            case NOT_FINISHED:
                return calcHorizontalScore(board)
                        + calcVerticalScore(board)
                        + calcDiagonalScore1(board)
                        + calcDiagonalScore2(board);

            default:
                return 0;
        }
    }

    private int calcHorizontalScore(final Board board) {
        myMaxCounter = 0;
        opponentMaxCounter = 0;

        //For each row...
        for (int row = 0; row < board.getHeight(); row++) {

            //Sum up the consecutive Stones in a row
            myCounter = 0;
            opponentCounter = 0;

            for (int column = 0; column < board.getWidth(); column++) {
                countForConsecutiveFields(board.getFields()[column][row], column, row);
            }
        }
        return getRating();
    }

    private int calcVerticalScore(final Board board) {
        myMaxCounter = 0;
        opponentMaxCounter = 0;

        //For each column
        for (int column = 0; column < board.getWidth(); column++) {

            myCounter = 0;
            opponentCounter = 0;
            //Sum up the consecutive Stones in a column
            for (int row = 0; row < board.getHeight(); row++) {
                countForConsecutiveFields(board.getFields()[column][row], column, row);
            }

        }
        return getRating();
    }

    /*
     * From top-left to bottom-right diagonals
     */
    private int calcDiagonalScore1(final Board board) {
        myMaxCounter = 0;
        opponentMaxCounter = 0;

        //Für Zeilen 0 - 4 (Spalte 0)
        for (int row = 0; row <= board.getHeight() - 2; row++) {

            myCounter = 0;
            opponentCounter = 0;
            //Nach rechts unten Zählen
            for (int i = 0; i <= row; i++) {
                countForConsecutiveFields(board.getFields()[i][row - i], i, row - i);
            }
        }

        //Für Spalten 0 - 6 (Zeile 5)
        for (int column = 0; column < board.getWidth(); column++) {

            myCounter = 0;
            opponentCounter = 0;
            //Nach rechts unten Zählen
            for (int i = 0; i <= Math.min(board.getHeight() - 1, board.getWidth() - 1 - column); i++) {
                countForConsecutiveFields(board.getFields()[column + i][board.getHeight() - 1 - i], column + i, board.getHeight() - 1 - i);
            }
        }
        return getRating();
    }

    /*
     * From bottom-left to top-right diagonals
     */
    private int calcDiagonalScore2(final Board board) {
        myMaxCounter = 0;
        opponentMaxCounter = 0;

        //Für jede Zeile
        for (int row = board.getHeight() - 1; row > 0; row--) {

            myCounter = 0;
            opponentCounter = 0;
            //Nach rechts oben Zählen
            for (int i = 0; i <= board.getHeight() - 1 - row; i++) {
                countForConsecutiveFields(board.getFields()[i][row + i], i, row + i);
            }
        }

        //Für jede Spalte
        for (int column = 0; column < board.getWidth(); column++) {

            myCounter = 0;
            opponentCounter = 0;
            //Nach rechts oben Zählen
            for (int i = 0; i <= Math.min(board.getWidth() - 1 - column, board.getHeight() - 1); i++) {
                countForConsecutiveFields(board.getFields()[column + i][i], i + column, i);
            }
        }
        return getRating();
    }

    private int getRating() {

        if (board.isMyTurn()) {
            return 2 * getExponentialRating(myMaxCounter) - getExponentialRating(opponentMaxCounter);
        } else {
            return getExponentialRating(myMaxCounter) - 2 * getExponentialRating(opponentMaxCounter);
        }

    }

    private int getExponentialRating(int count) {
        return Math.toIntExact(Math.round(Math.pow(10, count)));
    }

    private void countForConsecutiveFields(char symbol, int column, int row) {

        if (symbol == board.getMySymbol()) {
            myCounter++;
            opponentCounter = 0;
        }

        if (symbol == board.getOpponentSymbol()) {
            myCounter = 0;
            opponentCounter++;
        }

        if (symbol == board.getEmptySymbol() && !board.isMovePossible(column, row)) {
            myCounter = 0;
            opponentCounter = 0;
        }

        if (myCounter > myMaxCounter) {
            myMaxCounter = myCounter;
        }

        if (opponentCounter > opponentMaxCounter) {
            opponentMaxCounter = opponentCounter;
        }
    }

}
